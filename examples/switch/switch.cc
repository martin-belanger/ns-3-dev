/**
 Default Network Topology

  client1  server2
     |        |
 +---+--------+---+
 |     Switch     |
 +---+--------+---+
     |        |
  server1  client2
*/

#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/csma-module.h"
#include "ns3/internet-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/applications-module.h"
#include "ns3/ipv4-global-routing-helper.h"
#include "ns3/bridge-helper.h"

#define CSMA_LINK_DATA_RATE       "100Mbps"
#define CSMA_LINK_DELAY           "500ns"
#define UDP_ECHO_PORT             9
#define DEFAULT_SIM_DURATION_SEC  60

using namespace ns3;

NS_LOG_COMPONENT_DEFINE("simple-switch");

static void create_client_server(Ptr<Node>   server_node,
                 Ptr<Node>   client_node,
                 const char *server_addr,
                 int         duration_sec,
                 float       inter_pkt_interval,
                 uint32_t    pkt_size)
{
    UdpEchoServerHelper server(UDP_ECHO_PORT);

    ApplicationContainer serverApp = server.Install(server_node);
    serverApp.Start(Seconds(0.5));
    serverApp.Stop(Seconds(duration_sec));

    // Install UDP echo client on client2
    Time interPacketInterval = Seconds(inter_pkt_interval);
    uint32_t maxPacketCount = (duration_sec - 2.0) / 0.005;

    UdpEchoClientHelper client(Ipv4Address(server_addr), UDP_ECHO_PORT);

    client.SetAttribute("MaxPackets", UintegerValue(maxPacketCount));
    client.SetAttribute("Interval", TimeValue(interPacketInterval));
    client.SetAttribute("PacketSize", UintegerValue(pkt_size));

    ApplicationContainer clientApp = client.Install(client_node);
    clientApp.Start(Seconds(0.5));
    clientApp.Stop(Seconds(duration_sec));
}


int main(int argc, char *argv[])
{
    bool tron = false;
    bool verbose = false;
    bool halfduplex = false;
    int  sim_duration_sec = DEFAULT_SIM_DURATION_SEC;

    CommandLine cmd(__FILE__);
    cmd.AddValue("verbose", "Enable debug-level messages", verbose);
    cmd.AddValue("tron", "TRace ON - Enable full tracing", tron);
    cmd.AddValue("half-duplex", "Set transmit mode to half-duplex", halfduplex);
    cmd.AddValue("sec", "Duration of simulation in seconds", sim_duration_sec);
    cmd.Parse(argc, argv);

    if (tron)
    {
        LogLevel log_level = (LogLevel)((int)LOG_LEVEL_ALL | (int)LOG_PREFIX_ALL);
        LogComponentEnable("simple-switch", log_level);
        LogComponentEnable("CsmaNetDevice", log_level);
        LogComponentEnable("CsmaChannel", log_level);
        LogComponentEnable("Backoff", log_level);
    }
    else
    {
        LogLevel log_level = verbose ? LOG_LEVEL_DEBUG : LOG_LEVEL_INFO;
        LogComponentEnable("simple-switch", log_level);
    }

    if (sim_duration_sec <= 0)
        sim_duration_sec = DEFAULT_SIM_DURATION_SEC;

    std::string tx_mode = halfduplex ? "half-duplex" : "full-duplex";

    NS_LOG_INFO("Simulation duration: " << sim_duration_sec << "s");
    NS_LOG_INFO("Transmission:        " << tx_mode);
    NS_LOG_INFO("Data rate:           " << CSMA_LINK_DATA_RATE);
    NS_LOG_INFO("Delay:               " << CSMA_LINK_DELAY);

    NS_LOG_DEBUG("Creating nodes");
    Ptr<Node> server1 = CreateObject<Node>(); // 192.168.1.100
    Ptr<Node> client1 = CreateObject<Node>(); // 192.168.1.101
    Ptr<Node> server2 = CreateObject<Node>(); // 192.168.1.200
    Ptr<Node> client2 = CreateObject<Node>(); // 192.168.1.201
    Ptr<Node> sw      = CreateObject<Node>(); // n/a

    // Give the nodes names
    Names::Add("Echo server 1", server1);
    Names::Add("Echo client 1", client1);
    Names::Add("Echo server 2", server2);
    Names::Add("Echo client 2", client2);
    Names::Add("Switch/Bridge", sw);

    // Create CSMA link to use for connecting LAN nodes together
    NS_LOG_DEBUG("L2: Create CSMA link ("
             << tx_mode << ", "
             << "rate:" << CSMA_LINK_DATA_RATE << ", "
             << "delay:" << CSMA_LINK_DELAY << ")");

    CsmaHelper csma;
    csma.SetChannelAttribute("FullDuplexMode", BooleanValue(!halfduplex));
    csma.SetChannelAttribute("DataRate", StringValue(CSMA_LINK_DATA_RATE));
    csma.SetChannelAttribute("Delay", StringValue(CSMA_LINK_DELAY));

    // Now, connect nodes to switch
    NS_LOG_DEBUG("L2: Connect nodes to switch with " << tx_mode << " CSMA links");

    NetDeviceContainer server1_link = csma.Install(NodeContainer(server1, sw));
    NetDeviceContainer client1_link = csma.Install(NodeContainer(client1, sw));
    NetDeviceContainer server2_link = csma.Install(NodeContainer(server2, sw));
    NetDeviceContainer client2_link = csma.Install(NodeContainer(client2, sw));

    // Create the list of NetDevices for the switch
    NetDeviceContainer sw_netdev;
    sw_netdev.Add(server1_link.Get(1));
    sw_netdev.Add(client1_link.Get(1));
    sw_netdev.Add(server2_link.Get(1));
    sw_netdev.Add(client2_link.Get(1));

    // Install bridging code on the switch
    BridgeHelper bridge;
    bridge.Install(sw, sw_netdev);

    // Install the L3 internet stack (TCP/IP) on UDP endpoints
    NS_LOG_DEBUG("L3: Install the ns3 IP stack on udp client and server nodes");
    InternetStackHelper ns3IpStack;
    ns3IpStack.Install(NodeContainer(server1, client1, server2, client2));

    // Assign LAN IP addresses
    NetDeviceContainer echo1LanIpDevices;
    NetDeviceContainer echo2LanIpDevices;
    Ipv4AddressHelper ipv4;

    NS_LOG_DEBUG("L3: Assign LAN IP Addresses - server1:192.168.1.100, client1:192.168.1.101");
    echo1LanIpDevices.Add(server1_link.Get(0)); // NOTE: order matters here for
    echo1LanIpDevices.Add(client1_link.Get(0)); //       IP address assignment
    ipv4.SetBase("192.168.1.0", "255.255.255.0", "0.0.0.100");
    ipv4.Assign(echo1LanIpDevices);

    NS_LOG_DEBUG("L3: Assign LAN IP Addresses - server2:192.168.1.200, client2:192.168.1.201");
    echo2LanIpDevices.Add(server2_link.Get(0));
    echo2LanIpDevices.Add(client2_link.Get(0));
    ipv4.SetBase("192.168.1.0", "255.255.255.0", "0.0.0.200");
    ipv4.Assign(echo2LanIpDevices);

    NS_LOG_DEBUG("APP: Create client1-server1 apps");
    create_client_server(server1, client1, "192.168.1.100", sim_duration_sec, 0.005, 1000);

    NS_LOG_DEBUG("APP: Create client2-server2 apps");
    create_client_server(server2, client2, "192.168.1.200", sim_duration_sec, 0.005, 1000);

    // Configure PCAP traces
    NS_LOG_DEBUG("Configure PCAP Tracing");
    csma.EnablePcap("simple-switch-echo1.pcap", echo1LanIpDevices.Get(0), true, true);
    csma.EnablePcap("simple-switch-echo2.pcap", echo2LanIpDevices.Get(0), true, true);

    // Start the simulation.
    NS_LOG_DEBUG("Run Simulation for " << sim_duration_sec << " seconds.");

    Simulator::Stop(Seconds(sim_duration_sec));
    Simulator::Run();
    Simulator::Destroy();

    NS_LOG_DEBUG("Done");

    return 0;
}
